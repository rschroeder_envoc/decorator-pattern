namespace Decorator
{
    public class PercenageOfNetProfitBonus : IBonus
    {
        private readonly decimal percentage;
        private readonly decimal netProfit;
        private readonly IBonus bonus;

        public PercenageOfNetProfitBonus(decimal percentage, decimal netProfit, IBonus bonus)
        {
            this.percentage = percentage;
            this.netProfit = netProfit;
            this.bonus = bonus;
        }

        public decimal Amount
        {
            get { return bonus.Amount + (netProfit * percentage); }
        }
    }
}