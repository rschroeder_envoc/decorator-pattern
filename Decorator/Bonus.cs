namespace Decorator
{
    public class Bonus : IBonus
    {
        public Bonus(decimal amount)
        {
            Amount = amount;
        }

        public decimal Amount { get; private set; }
    }
}