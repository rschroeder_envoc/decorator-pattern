using System;
using System.Collections.Generic;
using System.Linq;

namespace Decorator
{
    public class TieredUnitBonus : IBonus
    {
        private readonly int unitCount;
        private readonly IBonus bonus;
        private List<Tier> tiers;

        public TieredUnitBonus(int unitCount, IBonus bonus, List<Tier> tiers)
        {
            this.unitCount = unitCount;
            this.bonus = bonus;
            this.tiers = tiers;
        }

        public decimal Amount
        {
            get { return bonus.Amount + GetAmount(); }
        }

        private decimal GetAmount()
        {
            var tier = tiers.FirstOrDefault(x => unitCount >= x.Low && unitCount <= x.High);
            if (tier != null)
            {
                return tier.BonusAmount;
            }
            throw new Exception();
        }
    }
}