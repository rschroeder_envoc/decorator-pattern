namespace Decorator
{
    public class Tier
    {
        public Tier(int low, int high, decimal bonusAmount)
        {
            Low = low;
            High = high;
            BonusAmount = bonusAmount;
        }

        public Tier()
        {
        }

        public int Low { get; set; }

        public int High { get; set; }

        public decimal BonusAmount { get; set; }
    }
}