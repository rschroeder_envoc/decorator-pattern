namespace Decorator
{
    public interface IBonus
    {
        decimal Amount { get; }
    }
}