﻿Feature: Bonus Calculations

Scenario: Bonus is 0 by default
    Given A bonus of 0
    Then the bonus amount is 0

Scenario: Bonus set to 10 returns 10
    Given A bonus of 10
    Then the bonus amount is 10

Scenario: Percentage of net profit bonus
    Given A bonus of 500
    And a percentage of net profit bonus with percentage of 10 and net profit 1000
    Then the bonus amount is 600

Scenario: Tiered Unit bonus
    Given A bonus of 500
    And a percentage of net profit bonus with percentage of 10 and net profit 1000
    And a tiered unit bonus with a unit count of 30 
    | Low | High | BonusAmount |
    | 0   | 10   | 100         |
    | 11  | 20   | 200         |
    | 21  | 30   | 300         |
    | 31  | 40   | 400         |
    Then the bonus amount is 900