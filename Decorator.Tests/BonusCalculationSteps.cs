﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Envoc.Core.UnitTests.Extensions;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Decorator.Tests
{
    [Binding]
    public class BonusCalculationSteps
    {
        private IBonus bonus;

        [Given(@"A bonus of (.*)")]
        public void GivenABonusOf_(decimal amount)
        {
            bonus = new Bonus(amount);
        }

        [Given(@"a percentage of net profit bonus with percentage of 10 and net profit 1000")]
        public void GivenAPercentageOfNetProfitBonusWithPercentageOf10AndNetProfit1000()
        {
            bonus = new PercenageOfNetProfitBonus(.10M, 1000, bonus);
        }

        [Given(@"a tiered unit bonus with a unit count of 30")]
        public void GivenATieredUnitBonusWithAUnitCountOf30(Table table)
        {
            var tiers = table.CreateSet<Tier>().ToList();
            bonus = new TieredUnitBonus(30, bonus, tiers);
        }

        [Then(@"the bonus amount is (.*)")]
        public void ThenTheBonusAmountIs_(decimal amount)
        {
            bonus.Amount.ShouldBe(amount);
        }
    }
}
